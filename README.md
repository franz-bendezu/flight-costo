# Flight FIIS

## App 
- [https://flightsfiis.web.app/](https://flightsfiis.web.app/)


## Team
- Bendezú Isidro, Franz Antony
- Chau Yu, Yao Peng
- Coaguila Pocco, Frank Billy
- Luna Garcia, Luis Alejandro

## Prerequisites

- node - at least v10.13 We recommend you have the latest LTS version installed.
- A text editor, we recommend VS Code with the Vetur extension or WebStorm
- A terminal, we recommend using VS Code's integrated terminal or WebStorm terminal.


## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
